.PHONY: install uninstall

install:
	$(shell for f in ServiceMenus/* ; do ln -s "$$(pwd)/$$f" ~/.local/share/kservices5/ServiceMenus ; done)

uninstall:
	$(shell for f in ServiceMenus/* ; do rm ~/.local/share/kservices5/"$$f" ; done)
